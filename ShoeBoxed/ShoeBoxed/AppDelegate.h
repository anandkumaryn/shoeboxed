//
//  AppDelegate.h
//  ShoeBoxed
//
//  Created by Anand kumar on 04/04/17.
//  Copyright © 2017 Anand kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


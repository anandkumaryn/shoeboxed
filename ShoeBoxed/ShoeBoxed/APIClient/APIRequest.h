//
//  APIRequest.h
//  banking
//
//  Created by Anand kumar on 07/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM (NSUInteger, APIRequestType)
{
    POST,
    PUT,
    DELETE,
    GET,
    PATCH
};


@interface APIRequest : NSObject

@property(nonatomic, copy) NSString *service;
@property(nonatomic) APIRequestType serviceType;
@property(nonatomic) NSDictionary *params;
@property(nonatomic) NSDictionary *additionalHeaders;
@property(nonatomic) int index;
@end

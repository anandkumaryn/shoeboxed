//
//  APIClient.m
//  banking
//
//  Created by Anand kumar on 07/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import "APIClient.h"
#import "OAuthManager.h"

@interface APIClient()
@property(nonatomic, copy) NSString * accessToken;
@end

@implementation APIClient

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.accessToken = [[OAuthManager sharedInstance] credential].oauthToken;
    }
    return self;
}

- (NSString *) getEndpointUrl {
    
    return (self.isZipLoop)? @"https://api.ziploop.com/partner/1/receipts?" : @"https://api.shoeboxed.com/v2/";
}

- (void) makeRequest:(APIRequest *)request withCallBack:(APIShoeBoxBlock)handler {
    
    NSURLRequest *reqUrl = [self generateDispatchableRequest:request];
     NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:reqUrl completionHandler:^(NSData * _Nullable data,__unused NSURLResponse * _Nullable response, NSError * _Nullable respError) {
        
        NSError *err;
        
        NSDictionary *json =  nil;
        if(data) {
            json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];

            NSLog(@"%@",json);
            
            handler(json, true, request.index ,nil);

        } else {
            err = respError;
            
            handler(nil, false, request.index ,err);

        }
        
    }];
    
    [task resume];
    
}


- (NSURLRequest *) generateDispatchableRequest:(APIRequest *) request {
    
    if(! _accessToken && !self.isZipLoop){
        return nil;
    }
    
    NSString *urlString = [[self getEndpointUrl] stringByAppendingString:request.service];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    if(url) {
        
        NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:url];
        
        switch (request.serviceType) {
                
            case GET :
            {
                urlRequest.HTTPMethod = @"GET";
                break;
            }
            case DELETE: {
                urlRequest.HTTPMethod = @"DELETE";
                NSError *error = nil;

                if(request.params.count){
                    urlRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:request.params options:0 error:&error];
                }

                break;
            }
                
            default: {
                
                NSError *error = nil;

                urlRequest.HTTPMethod = (request.serviceType == POST) ? @"POST" : (request.serviceType == PUT) ? @"PUT" : @"PATCH";
                
                urlRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:request.params options:0 error:&error];

                break;
            }
        }
        
        [urlRequest setValue:[NSString stringWithFormat:@"Bearer %@",_accessToken] forHTTPHeaderField:@"Authorization"];
        [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

        return urlRequest;
        
    } else {
        
        return nil;
    }

}

@end

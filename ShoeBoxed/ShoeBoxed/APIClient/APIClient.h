//
//  APIClient.h
//  banking
//
//  Created by Anand kumar on 07/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIRequest.h"

typedef void (^APIShoeBoxBlock)(id result, BOOL success , int index, NSError *error);
typedef void (^APIZiploopBlock)(id result, BOOL success , int index, NSError *error);


@interface APIClient : NSObject

@property(nonatomic, assign) BOOL isZipLoop;

- (void) makeRequest:(APIRequest *)request withCallBack:(APIShoeBoxBlock)handler;
- (NSString *) getEndpointUrl;
@end

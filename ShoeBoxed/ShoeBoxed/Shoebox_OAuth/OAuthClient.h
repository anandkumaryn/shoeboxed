//
//  OAuthClient.h
//  banking
//
//  Created by Anand kumar on 07/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OAuthClient : NSObject

@property (nonatomic, copy) NSString *oauthToken;
@property (nonatomic, copy) NSString *oauthRefreshToken;
@property (nonatomic, strong) NSDate   *aouthTokenExpiresAt;
@property (nonatomic, copy) NSString *oauthCode;
@property (nonatomic, copy) NSString *clientId;
@property (nonatomic, copy) NSString *secretKey;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *redirectUrl;
@property (nonatomic, copy) NSString *tokenType;
@property (nonatomic, copy) NSString *scope;
@property (nonatomic, assign) BOOL isExpired;

@end

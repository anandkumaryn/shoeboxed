//
//  OAuthManager.m
//  banking
//
//  Created by Anand kumar on 07/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import "OAuthManager.h"
#import "OAuthWebViewController.h"
#import "NSString+String.h"

static NSString* const KAUTHORIZEURL  = @"https://id.shoeboxed.com/oauth/authorize?";
static NSString* const KACCESSTOKENURL  = @"https://id.shoeboxed.com/oauth/token";
static NSString* const KREDIRECTURL  = @"https://ymedialabs.com";
static NSString* const KRESPONETYPE = @"code";  // for mobile app we use "token" & for web use "code"


static NSString* const KOAUTHACCESSTOKEN = @"access_token";
static NSString* const KOAUTHREFRESHTOKEN = @"refresh_token";
static NSString* const KOAUTHEXPIREDATE = @"expires_in";



typedef NS_ENUM (NSUInteger, APIContentType)
{
    APPLICATIONJSON   = 1,
    APPLICATIONFORM  = 2
};

@interface OAuthManager() {
    
    NSString * consumerKey;
    NSString * consumerSecret;
}

@end

@interface OAuthManager()
@property(nonatomic) OAuthShoeBoxBlock  completionHanlder;

@end


@implementation OAuthManager

+ (id)sharedInstance
{
    static OAuthManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance initialSetup];
        
    });
    return sharedInstance;
}

- (void)initialSetup {
    
    consumerKey = @"3eab03f1c7014b298011636e07faeb6d";
    consumerSecret = @"YHu7W4RuwrEVEWXKBnvD/Otqoxn7qb2yMxiINzKWHyto8zsw43S0m";
    
    _credential = [[OAuthClient alloc] init];
    
    NSUserDefaults *defaultManager = [NSUserDefaults standardUserDefaults];
    
    if([defaultManager objectForKey:KOAUTHACCESSTOKEN]){
        _credential.oauthToken = [defaultManager objectForKey:KOAUTHACCESSTOKEN];
    }
    if([defaultManager objectForKey:KOAUTHREFRESHTOKEN]){
        _credential.oauthRefreshToken = [defaultManager objectForKey:KOAUTHREFRESHTOKEN];
    }
    
    if([defaultManager objectForKey:KOAUTHEXPIREDATE]){
        _credential.aouthTokenExpiresAt = (NSDate *) [defaultManager objectForKey:KOAUTHEXPIREDATE];
        if ([_credential.aouthTokenExpiresAt compare:[NSDate date]] == NSOrderedDescending) {
            _credential.isExpired = false;
        }
    }
}

- (BOOL)isTokenExpired {
    
    BOOL isExpired = true;
    
    if ([_credential.aouthTokenExpiresAt compare:[NSDate date]] == NSOrderedDescending) {
        isExpired = false;
    }
    
    return isExpired;
}

- (void)authorizeShoeBoxFor:(UIViewController *)controller withScope:(NSString *)scope state:(NSString *)state forParameter:(NSDictionary *)params withCompletionHandler:(OAuthShoeBoxBlock)handler {
    
    if( [self isTokenExpired] == false) {
        handler(self.credential,true ,nil);
    }
    
    _completionHanlder = handler;
    
    NSString *queryString = [self consturctQueryFor:scope forState:state withParameter:params];
    
    OAuthWebViewController *webVC = [[OAuthWebViewController alloc] initWithNibName:@"OAuthWebViewController" bundle:nil];
    
    [controller presentViewController:webVC animated:YES completion:nil];
    
    NSString *targetUrlString = [KAUTHORIZEURL stringByAppendingString:queryString];
    
    [webVC loadRequestUrl:[NSURL URLWithString:targetUrlString] forScheme:nil withRedirectionUrlString:KREDIRECTURL withCompletionHandler:^(NSURL * url) {
        [self parseOAuthCallbackURL:url];
    }];
    
}

- (void) obtainRefreshedAccessTokenWithCompletionHandler:(OAuthShoeBoxBlock)handler {
    
    if( [self isTokenExpired] == false) {
        handler(self.credential,true ,nil);
    }
    
    _completionHanlder = handler;
    
    NSDictionary * parameters = @{@"refresh_token": _credential.oauthRefreshToken, @"grant_type": @"refresh_token", @"client_id" : consumerKey, @"client_secret" : consumerSecret};
    
    [self performAccessTokenRequestForContentType:APPLICATIONFORM contentString:@"application/x-www-form-urlencoded" withParamter:parameters];
    
}

- (void) obtainAccessToken:(APIContentType) contType{
    
    NSString *contentType = (contType == APPLICATIONJSON) ? @"application/json" : @"application/x-www-form-urlencoded";
    
    NSDictionary * parameters = @{@"code": _credential.oauthCode , @"grant_type": @"authorization_code",
                       @"redirect_uri" : KREDIRECTURL, @"client_id" : consumerKey, @"client_secret" : consumerSecret};
    [self performAccessTokenRequestForContentType:contType contentString:contentType withParamter:parameters];
    
}

-(void) performAccessTokenRequestForContentType:(APIContentType)type  contentString:(NSString *)contentType withParamter:(NSDictionary *)param {
    
    NSURL *url = [NSURL URLWithString:KACCESSTOKENURL];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    
    [request setValue:contentType forHTTPHeaderField:@"content-type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSError *error = nil;

    if(type == APPLICATIONJSON) {
        request.HTTPBody = [NSJSONSerialization dataWithJSONObject:param options:0 error:&error];
    }
    else{
        
        NSString *queryString = [self generateQueryString:param];
        NSData *data = [queryString dataUsingEncoding:NSUTF8StringEncoding];
        
        [request setValue:[NSString stringWithFormat:@"%ld",(unsigned long)data.length] forHTTPHeaderField:@"Content-Length"];
        request.HTTPBody = data;

    }
    
   NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data,__unused NSURLResponse * _Nullable response, NSError * _Nullable respError) {
       
       NSError *err;
       
       NSDictionary *json =  nil;
       if(data) {
           json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
       [self parseResponse:json];

       } else {
           err = respError;
           
           if(self.completionHanlder) {
               self.completionHanlder (nil,false , err);
               self.completionHanlder =  nil;
           }
       }
       
       
   }];
    
   [task resume];
    
}


- (NSString *) generateQueryString:(NSDictionary *)param {
    
    NSString *queryString = @"";
    
    for (NSString *key in param.allKeys) {
        
        if(queryString.length == 0) {
            queryString = [NSString stringWithFormat:@"%@=%@",key,param[key]];
        } else {
            NSString *qStr = [NSString stringWithFormat:@"&%@=%@",key,param[key]];
            queryString = [queryString stringByAppendingString:qStr];
        }
    }
    
    return queryString;
}

- (NSString *) consturctQueryFor:(NSString *)scope forState:(NSString *)state withParameter:(NSDictionary *)params {
    
    NSString *queryString = [NSString stringWithFormat:@"client_id=%@&redirect_uri=%@&response_type=%@",consumerKey,KREDIRECTURL,KRESPONETYPE];
    
    if(scope){
        NSString *scopeString = [@"&scope=" stringByAppendingString:scope];
        queryString = [queryString stringByAppendingString:scopeString];
    }
    
    if(state){
        
        NSString *stateString = [@"&state=" stringByAppendingString:state];
        queryString = [queryString stringByAppendingString:stateString];
    }
    
    for (NSString *key in params.allKeys) {
        
        NSString *paramString = [NSString stringWithFormat:@"&%@=%@",key,params[key]];
        
        queryString = [queryString stringByAppendingString:paramString];
    }
   
    
    return queryString;
}


- (void) parseOAuthCallbackURL:(NSURL *)url {
    
    if(!url) {
        return;
    }
    
    NSMutableDictionary * responseparam = [[NSMutableDictionary alloc] init];
    
    if(url.query) {
        [responseparam setDictionary:[url.query parametersFromQueryString]];
    }
    
    if(url.fragment && ![url.fragment isEqualToString:@""]) {
        [responseparam setDictionary:[url.fragment parametersFromQueryString]];
    }
    
    [self parseResponse:responseparam];
}

- (void) parseResponse:(NSDictionary *) response {
    
  //  NSLog(@"%@", response);
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    if (response[@"access_token"]){
        
        NSString *token = [response[@"access_token"] safeStringByRemovingPercentEncoding];
        _credential.oauthToken = token;
        
        [userDefault setObject:token forKey:KOAUTHACCESSTOKEN];
        
        if(response[@"expires_in"]){
            
            NSNumber *number = response[@"expires_in"];
            
            if([number isKindOfClass:[NSNumber class]]) {
                _credential.aouthTokenExpiresAt = [NSDate dateWithTimeInterval:number.doubleValue sinceDate:[NSDate date]];
            }else {
                _credential.aouthTokenExpiresAt = [NSDate dateWithTimeInterval:number.doubleValue sinceDate:[NSDate date]];
            }
            
            _credential.isExpired = false;
            
            [userDefault setObject:_credential.aouthTokenExpiresAt forKey:KOAUTHEXPIREDATE];
        }
        
        if(response[@"refresh_token"]) {
            
            _credential.oauthRefreshToken = [response[@"refresh_token"] safeStringByRemovingPercentEncoding];
            [userDefault setObject:_credential.oauthRefreshToken forKey:KOAUTHREFRESHTOKEN];

        }
        
        [userDefault synchronize];
        
        if(_completionHanlder) {
            _completionHanlder (self.credential,true , nil);
            _completionHanlder =  nil;
        }
    }
    else if(response[@"code"]) {
        _credential.oauthCode = [response[@"code"] safeStringByRemovingPercentEncoding];
        [self obtainAccessToken:APPLICATIONFORM];
    }
    else if(response[@"error"]) {
        NSString * description = response[@"error_description"];
        NSError *error = [NSError errorWithDomain:description code:400 userInfo:response];

        if(_completionHanlder) {
            _completionHanlder (nil,false , error);
            _completionHanlder =  nil;
        }
    } else {
        
        NSString * message = @"No access_token, no code and no error provided by server";
        NSError *error = [NSError errorWithDomain:message code:400 userInfo:response];
        
        if(_completionHanlder) {
            _completionHanlder (nil,false , error);
            _completionHanlder =  nil;
        }
    }
    
}


- (NSString *) generateStateWithLenght:(int)len {
    
    NSString * letter = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSUInteger length = letter.length;
    
    NSString *randomString = @"";
    
    for (int i= 0; i < (int)len; i++) {
        NSUInteger rand = arc4random_uniform((uint32_t)length);
        NSString * character  = [NSString stringWithFormat:@"%C",[letter characterAtIndex:rand]] ;
        randomString = [randomString stringByAppendingString: character];
        
    }
    
    return randomString;
    
}
@end

//
//  NSString+String.m
//  banking
//
//  Created by Anand kumar on 07/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import "NSString+String.h"

@implementation NSString (String)

- (NSString *) safeStringByRemovingPercentEncoding {
    
    return [self stringByRemovingPercentEncoding];
}

-(NSDictionary *) parametersFromQueryString {
    
    return  [self dictionaryBySplitting:@"&" withKeyValueSeparator:@"="];
}

- (NSDictionary *) dictionaryBySplitting:(NSString *)elementSeparator withKeyValueSeparator:(NSString *)keyValueSeparator {
    
    NSString *string = self;
    
    if ([self hasPrefix:elementSeparator]) {
        string = [self substringFromIndex:1];
    }
    
    NSScanner *scanner = [[NSScanner alloc] initWithString:string];
    NSString *key;
    NSString *value;
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    while (!scanner.isAtEnd) {
        
        key = nil;
        [scanner scanUpToString:keyValueSeparator intoString:&key];
        [scanner scanString:keyValueSeparator intoString:nil];
        
        value = nil;
        
        [scanner scanUpToString:elementSeparator intoString:&value];
        [scanner scanString:elementSeparator intoString:nil];
        
        if(key && value){
            [dict setValue:value forKeyPath:key];
        }
        
    }
    
    return dict;
    
}
@end

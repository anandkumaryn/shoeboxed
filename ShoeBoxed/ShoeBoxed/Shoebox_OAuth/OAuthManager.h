//
//  OAuthManager.h
//  banking
//
//  Created by Anand kumar on 07/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "OAuthClient.h"
#import "SBUser.h"

typedef void (^OAuthShoeBoxBlock)(OAuthClient *result, BOOL success , NSError *error);

@interface OAuthManager : NSObject

@property(nonatomic, strong) OAuthClient * credential;
@property(nonatomic, strong) SBUser *userInfo;

+ (id)sharedInstance;

- (BOOL) isTokenExpired;
- (NSString *) generateStateWithLenght:(int)len;


- (void)authorizeShoeBoxFor:(UIViewController *)controller withScope:(NSString *)scope state:(NSString *)state forParameter:(NSDictionary *)params withCompletionHandler:(OAuthShoeBoxBlock)handler;

- (void) obtainRefreshedAccessTokenWithCompletionHandler:(OAuthShoeBoxBlock)handler;

@end

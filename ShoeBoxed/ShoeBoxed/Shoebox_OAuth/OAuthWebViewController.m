//
//  OAuthWebViewController.m
//  banking
//
//  Created by Anand kumar on 07/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import "OAuthWebViewController.h"

@interface OAuthWebViewController ()<UIWebViewDelegate> {
    
    void (^_completionHandler)(NSURL * responseUrl);

}

@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property(nonatomic, copy) NSString *urlScheme;
@property(nonatomic, copy) NSString *reDirectUrlString;
@property (nonatomic, strong) NSURLRequest *request;

@end

@implementation OAuthWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.webview loadRequest:_request];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadRequestUrl:(NSURL *)targetUrl forScheme:(NSString *)scheme withRedirectionUrlString:(NSString *)redirectionUrlString withCompletionHandler:(void(^)(NSURL *))handler {
    
    if(targetUrl) {
        
        _completionHandler = handler;

        self.urlScheme = scheme;
        self.reDirectUrlString = redirectionUrlString;
       self.request = [[NSURLRequest alloc] initWithURL:targetUrl];
        //[self.webview setDelegate:self];
    }
}

- (BOOL)webView:(UIWebView *)__unused webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)__unused navigationType {
    
    if ([request.URL.scheme isEqualToString:self.urlScheme]){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if ([request.URL.absoluteString containsString:[NSString stringWithFormat:@"%@/",self.reDirectUrlString]]) {
        
        if(_completionHandler){
            _completionHandler(request.URL);
            _completionHandler = nil;
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }

    
    return true;
}

@end

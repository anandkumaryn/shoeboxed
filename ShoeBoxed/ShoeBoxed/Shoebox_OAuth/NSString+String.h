//
//  NSString+String.h
//  banking
//
//  Created by Anand kumar on 07/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (String)
-(NSDictionary *) parametersFromQueryString;
- (NSString *) safeStringByRemovingPercentEncoding;

@end

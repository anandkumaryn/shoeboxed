//
//  OAuthWebViewController.h
//  banking
//
//  Created by Anand kumar on 07/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OAuthWebViewController : UIViewController

- (void)loadRequestUrl:(NSURL *)targetUrl forScheme:(NSString *)scheme withRedirectionUrlString:(NSString *)redirectionUrlString withCompletionHandler:(void(^)(NSURL *))handler;

@end

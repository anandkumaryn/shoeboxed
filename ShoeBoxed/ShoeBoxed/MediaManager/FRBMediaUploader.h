//
//  FRBMediaUploader.h
//  banking
//
//  Created by Anand kumar on 13/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MediaApi.h"


typedef void (^imageCompletionHandler)(id responseObject , NSError *error);

@interface FRBMediaUploader : NSObject<NSURLSessionDelegate>

+(void) uploadImageUrl:(NSURL *)filePathUrl withRequest:(MediaApi *)requestApi  withHandler:(imageCompletionHandler)callBack;

-(void) backGroundUploadImageUrl:(NSURL *)filePathUrl serverUrl:(NSString *)preSignedUrl withHandler:(imageCompletionHandler)callBack;

+ (UIImage *)imageWithImage:(UIImage *)image scaledToMaxWidth:(CGFloat)width maxHeight:(CGFloat)height;

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)size;

@end

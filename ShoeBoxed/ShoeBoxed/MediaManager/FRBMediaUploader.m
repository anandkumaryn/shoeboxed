//
//  FRBMediaUploader.m
//  banking
//
//  Created by Anand kumar on 13/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import "FRBMediaUploader.h"
#import "OAuthManager.h"

@implementation FRBMediaUploader

+(void) uploadImageUrl:(NSURL *)filePathUrl withRequest:(MediaApi *)requestApi  withHandler:(imageCompletionHandler)callBack{
    
    NSString *urlString = [@"https://api.shoeboxed.com/v2/" stringByAppendingString:requestApi.service];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    [request setHTTPMethod:@"POST"];
    
    
    NSString *boundary = @"14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    //-- For Sending text
    
    for (NSString *key in requestApi.params.allKeys) {
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",requestApi.params[key]] dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    //-- For sending image into service if needed (send image as imagedata)
    NSString * timestamp = [NSString stringWithFormat:@"%0.0f",[[NSDate date] timeIntervalSince1970] * 1000];
    
    NSData * imageDta = [NSData dataWithContentsOfURL:filePathUrl];

    UIImage *img = [UIImage imageWithData:imageDta];
    
    
    NSData *imageData  = UIImageJPEGRepresentation(img, 1.0);

    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition:form-data; name=\"attachment\"; filename=\"%@.jpg\"\r\n",timestamp] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //-- Sending data into server through URL
    [request setHTTPBody:body];
    
    
    NSString *accessToken =  [[OAuthManager sharedInstance] credential].oauthToken;

    [request setValue:[NSString stringWithFormat:@"Bearer %@",accessToken] forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSURLSession *session = [NSURLSession sharedSession];

    NSURLSessionDataTask *uploadtask  =  [session dataTaskWithRequest:request completionHandler:^(NSData *  data, NSURLResponse *  response, NSError *  error) {
        
        NSLog(@"response = %@",response);
        NSLog(@"error = %@",error);
        NSLog(@"data = %@",data);
        
        NSDictionary *json =  nil;
        NSError *err;

        if(data) {
            json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        }
        callBack(json,error);
        
    }];
    
    [uploadtask resume];
    
    
}

-(void) backGroundUploadImageUrl:(NSURL *)filePathUrl serverUrl:(NSString *)preSignedUrl withHandler:(imageCompletionHandler)callBack {
    
    NSURLSessionConfiguration *backgroundConfiguration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:[[NSUUID UUID] UUIDString]];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:backgroundConfiguration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:preSignedUrl]];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    [request setHTTPMethod:@"PUT"];
    [request setValue:@"image/jpeg" forHTTPHeaderField:@"Content-Type"];
    
    NSURLSessionUploadTask *task = [session uploadTaskWithRequest:request fromFile:filePathUrl completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSLog(@"response = %@",response);
        NSLog(@"error = %@",error);
        NSLog(@"data = %@",data);
        callBack(data,error);
        
    }];
    
    [task resume];
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)size {
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToMaxWidth:(CGFloat)width maxHeight:(CGFloat)height {
    CGFloat oldWidth = image.size.width;
    CGFloat oldHeight = image.size.height;
    
    CGFloat scaleFactor = (oldWidth > oldHeight) ? width / oldWidth : height / oldHeight;
    
    CGFloat newHeight = oldHeight * scaleFactor;
    CGFloat newWidth = oldWidth * scaleFactor;
    CGSize newSize = CGSizeMake(newWidth, newHeight);
    
    return [self imageWithImage:image scaledToSize:newSize];
}


@end

//
//  UserApi.m
//  banking
//
//  Created by Anand kumar on 07/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import "UserApi.h"

@implementation UserApi

- (instancetype)init
{
    self = [super init];
    if (self) {
        _requestType = GET;
        _serviceName = @"user";
    }
    return self;
}

- (APIRequestType) serviceType {
    return _requestType;
}

- (NSString *) service {
    return _serviceName;
}

- (NSDictionary *) params {
    return _parameters;
}

@end

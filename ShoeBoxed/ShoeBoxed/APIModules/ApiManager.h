//
//  ApiManager.h
//  banking
//
//  Created by Dimple on 07/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIClient.h"


@interface ApiManager : NSObject

+ (void) createUserAccounts:(NSString*) countryCode forAccountName:(NSString *) accountName withHandler: (APIShoeBoxBlock)handler;
+ (void) createAccount:(NSString*)countryCode forAccountName:(NSString*)accountName withHandler:(APIShoeBoxBlock)handler;
+ (void) createAccountUsers:(NSString*)accountId userEmail:(NSString*)email userName:(NSString*)name userSurName:(NSString*)surName withHandler:(APIShoeBoxBlock)handler;
+ (void) createAccountCategories:(NSString*)accountId description:(NSString*)description name:(NSString*)name withHandler:(APIShoeBoxBlock)handler;

+ (void) getUserInfo:(APIShoeBoxBlock)handler;
+ (void) getUserAccountInfo:(APIShoeBoxBlock)handler;
+ (void) getUserApplicationInfo:(APIShoeBoxBlock)handler;
+ (void) getUserStats:(APIShoeBoxBlock)handler;

+ (void) getAccountInfo:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler;
+ (void) getAccountSettings:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler;
+ (void) getAccountUsers:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler;
+ (void) getAccountVendors:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler;
+ (void) getAccountBarCode:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler;
+ (void) getAccountBillingInfo:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler;
+ (void) getAccountCategories:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler;
+ (void) getAccountCoverSheet:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler;
+ (void) getAccountCreditCards:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler;
+ (void) getAccountCurrencies:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler;
+ (void) getAccountDocumentsForId:(NSString *)accountId forIndex:(int)index withHandler:(APIShoeBoxBlock)handler;
+ (void) getAccountEmails:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler;
+ (void) getAccountEnvelopes:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler;
+ (void) getAccountExports:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler;
+ (void) getAccountBillingInvoices:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler;
+ (void) getAccountLocales:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler;
+ (void) getAccountPlan:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler;

+ (void) deleteAccount:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler;
+ (void) deleteAccountUser:(NSString*)accountId forUserId:(NSString*)userId withHandler:(APIShoeBoxBlock)handler;
+ (void) deleteDocumentid:(NSString *)docId forAccountId:(NSString *)accountId withHandler:(APIShoeBoxBlock)handler;

@end

//
//  AccountApi.m
//  banking
//
//  Created by Dimple on 07/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import "AccountApi.h"

@implementation AccountApi

- (instancetype)init
{
    self = [super init];
    if (self) {
        _requestType = GET;
        _serviceName = @"accounts";
    }
    return self;
}

- (APIRequestType) serviceType {
    return _requestType;
}

- (NSString *) service {
    return _serviceName;
}

- (NSDictionary *) params {
    return _parameters;
}

@end

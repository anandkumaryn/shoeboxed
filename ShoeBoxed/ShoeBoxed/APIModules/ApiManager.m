//
//  ApiManager.m
//  banking
//
//  Created by Dimple on 07/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import "ApiManager.h"
#import "UserApi.h"
#import "AccountApi.h"

@implementation ApiManager

// MARK: User POST methods
+ (void) createUserAccounts:(NSString*) countryCode forAccountName:(NSString *) accountName withHandler: (APIShoeBoxBlock)handler {
    
    UserApi *userRequest = [[UserApi alloc] init];
    userRequest.requestType = POST;
    userRequest.serviceName = @"user/accounts";
    userRequest.parameters = @{@"countryCode": countryCode , @"slug": accountName};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:userRequest withCallBack:handler];
    
}

// MARK:  Accounts POST Methoods

+ (void) createAccount:(NSString*)countryCode forAccountName:(NSString*)accountName withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.requestType = POST;
    accountRequest.serviceName = @"accounts";
    accountRequest.parameters = @{@"countryCode": countryCode, @"slug": accountName};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

+ (void) createAccountUsers:(NSString*)accountId userEmail:(NSString*)email userName:(NSString*)name userSurName:(NSString*)surName withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.requestType = POST;
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/users",accountId];
    accountRequest.parameters = @{@"email": email, @"name": name, @"surname": surName};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

+ (void) createAccountCategories:(NSString*)accountId description:(NSString*)description name:(NSString*)name withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.requestType = POST;
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/categories",accountId];
    accountRequest.parameters = @{@"path": accountId , @"description": description , @"name": name};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

// MARK:- User GET methods
+ (void) getUserInfo:(APIShoeBoxBlock)handler {
    
    UserApi *userRequest = [[UserApi alloc] init];
    userRequest.serviceName = @"user";
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:userRequest withCallBack:handler];
    
}

+ (void) getUserAccountInfo:(APIShoeBoxBlock)handler {
    
    UserApi *userRequest = [[UserApi alloc] init];
    userRequest.serviceName = @"user/accounts";
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:userRequest withCallBack:handler];
    
}

+ (void) getUserApplicationInfo:(APIShoeBoxBlock)handler {
    
    UserApi *userRequest = [[UserApi alloc] init];
    userRequest.serviceName = @"user/applications";
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:userRequest withCallBack:handler];
    
}

+ (void) getUserStats:(APIShoeBoxBlock)handler {
    
    UserApi *userRequest = [[UserApi alloc] init];
    userRequest.serviceName = @"user/stats";
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:userRequest withCallBack:handler];
    
}


// MARK: - Accounts GET Methoods

+ (void) getAccountInfo:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@",accountId];    accountRequest.parameters = @{@"path": accountId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

+ (void) getAccountSettings:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/settings",accountId];     accountRequest.parameters = @{@"path": accountId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

+ (void) getAccountUsers:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/users",accountId];
    accountRequest.parameters = @{@"path": accountId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

+ (void) getAccountVendors:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/vendors",accountId];
    accountRequest.parameters = @{@"path": accountId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

// not working
+ (void) getAccountBarCode:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/barcode",accountId];
    accountRequest.parameters = @{@"path": accountId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

// not working
+ (void) getAccountBillingInfo:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/billing-info",accountId];
    accountRequest.parameters = @{@"path": accountId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

+ (void) getAccountCategories:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/categories",accountId];
    accountRequest.parameters = @{@"path": accountId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

// not working
+ (void) getAccountCoverSheet:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/coversheet",accountId];
    accountRequest.parameters = @{@"path": accountId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

+ (void) getAccountCreditCards:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/credit-cards",accountId];
    accountRequest.parameters = @{@"path": accountId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

+ (void) getAccountCurrencies:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/currencies",accountId];
    accountRequest.parameters = @{@"path": accountId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

+ (void) getAccountDocumentsForId:(NSString *)accountId forIndex:(int)index withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/documents",accountId];
    accountRequest.parameters = @{@"path": accountId};
    accountRequest.index = index;
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

// not working
+ (void) getAccountEmails:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/emails",accountId];
    accountRequest.parameters = @{@"path": accountId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

+ (void) getAccountEnvelopes:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/envelopes",accountId];
    accountRequest.parameters = @{@"path": accountId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

+ (void) getAccountExports:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/exports",accountId];
    accountRequest.parameters = @{@"path": accountId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

+ (void) getAccountBillingInvoices:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/invoices",accountId];
    accountRequest.parameters = @{@"path": accountId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

+ (void) getAccountLocales:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/locales",accountId];
    accountRequest.parameters = @{@"path": accountId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

+ (void) getAccountPlan:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/plan",accountId];
    accountRequest.parameters = @{@"path": accountId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}


// MARK:-  Accounts DELETE Methoods

+ (void) deleteAccount:(NSString*)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.requestType = DELETE;
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@",accountId];
    accountRequest.parameters = @{@"path": accountId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

+ (void) deleteAccountUser:(NSString*)accountId forUserId:(NSString*)userId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.requestType = DELETE;
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/users",accountId];
    accountRequest.parameters = @{@"account": accountId, @"user": userId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];
    
}

+ (void) deleteDocumentid:(NSString *)docId forAccountId:(NSString *)accountId withHandler:(APIShoeBoxBlock)handler {
    
    AccountApi *accountRequest = [[AccountApi alloc] init];
    accountRequest.requestType = DELETE;
    accountRequest.serviceName = [NSString stringWithFormat:@"accounts/%@/documents/%@",accountId,docId];
    accountRequest.parameters = @{@"account": accountId, @"id": docId};
    
    APIClient *apiCall = [[APIClient alloc] init];
    
    [apiCall  makeRequest:accountRequest withCallBack:handler];

}

@end

//
//  AccountApi.h
//  banking
//
//  Created by Dimple on 07/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import "APIRequest.h"

@interface AccountApi : APIRequest

@property (nonatomic, copy) NSString *serviceName;
@property (nonatomic, strong) NSDictionary *parameters;
@property (nonatomic) APIRequestType requestType;

@end

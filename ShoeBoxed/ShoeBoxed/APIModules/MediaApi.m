//
//  MediaApi.m
//  banking
//
//  Created by Anand kumar on 13/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import "MediaApi.h"

@implementation MediaApi

- (instancetype)init
{
    self = [super init];
    if (self) {
        _requestType = POST;
        _serviceName = @"accounts";
    }
    return self;
}

- (APIRequestType) serviceType {
    return _requestType;
}

- (NSString *) service {
    return _serviceName;
}

- (NSDictionary *) params {
    return _parameters;
}

@end

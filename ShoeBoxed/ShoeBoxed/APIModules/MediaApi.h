//
//  MediaApi.h
//  banking
//
//  Created by Anand kumar on 13/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import "APIRequest.h"

@interface MediaApi : APIRequest

@property (nonatomic, copy) NSString *serviceName;
@property (nonatomic, strong) NSDictionary *parameters;
@property (nonatomic) APIRequestType requestType;


@end

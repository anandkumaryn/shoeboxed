//
//  ViewController.m
//  ShoeBoxed
//
//  Created by Anand kumar on 04/04/17.
//  Copyright © 2017 Anand kumar. All rights reserved.
//

#import "ViewController.h"
#import "OAuthManager.h"
#import "ApiManager.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self performShoeBoxAuthorization];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - ShoeBox

- (void) performShoeBoxAuthorization {
    
    OAuthManager *manager = [OAuthManager sharedInstance];
    
    if (!manager.credential.oauthToken && !manager.credential.oauthCode) {
        
        NSString *state = [manager generateStateWithLenght:20];
        
        [manager authorizeShoeBoxFor:self withScope:@"all" state:state forParameter:nil withCompletionHandler:^(__unused OAuthClient *result, BOOL success,__unused NSError *error) {
            
            if(success){
                [self loadAllReceipts];
            } else {
                NSLog(@"%@", error);
                
            }
        }];
    }
    else if( [manager isTokenExpired] == true && !manager.credential.oauthCode ){
        
        [manager obtainRefreshedAccessTokenWithCompletionHandler:^(__unused OAuthClient *result, BOOL success, __unused NSError *error) {
            
            if(success){
                [self loadAllReceipts];
            } else {
                NSLog(@"%@", error);
            }
            
        }];
    } else if(manager.credential.oauthToken){
        [self loadAllReceipts];
    }
}

-(void) loadAllReceipts {
    
    
    OAuthManager *manager = [OAuthManager sharedInstance];
    
    if(!manager.userInfo){
        
        [ApiManager getUserInfo:^(id result, BOOL success,__unused int index,__unused NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(success){
                    manager.userInfo = [[SBUser alloc]initWithDictionary:result];
                } else {
                }
                
            });
            
        }];
        
    } else {
        
        //Retrive Userinfo & user Account from Manager Object
        
        NSLog(@"%@", manager.userInfo.accounts);
      
        
        
    }
    
}



@end

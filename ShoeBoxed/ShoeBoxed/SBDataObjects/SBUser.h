//
//  SBUser.h
//  banking
//
//  Created by Dimple on 08/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBUserAccount.h"

static  NSString * const kUserId = @"id";
static  NSString * const kUserEmail = @"email";
static  NSString * const kUserFirstName = @"firstName";
static  NSString * const kUserAccounts = @"accounts";
static  NSString * const kUserLastName = @"lastName";
static  NSString * const kUserName = @"name";
static  NSString * const kUserPhone = @"phone";
static  NSString * const kUserWorkPhone = @"workPhone";
static  NSString * const kUserOauth = @"oauth";
static  NSString * const kUserStaff = @"staff";
static  NSString * const kUserCellPhone = @"cellPhone";
static  NSString * const kUserConfirmed= @"confirmed";
static  NSString * const kUserSurname= @"surname";

@interface SBUser : NSObject

@property (nonatomic, assign) NSString *userid;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, strong) NSArray *accounts;
@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *workPhone;
@property (nonatomic, copy) NSString *oauth;
@property (nonatomic, copy) NSString *staff;
@property (nonatomic, copy) NSString *cellPhone;
@property (nonatomic, assign) int confirmed;
@property (nonatomic, copy) NSString *surname;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

- (instancetype)initZiploopWithDictionary:(NSArray *)receiptList;

@end

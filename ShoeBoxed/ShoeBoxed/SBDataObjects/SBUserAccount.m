//
//  SBUserAccount.m
//  banking
//
//  Created by Dimple on 08/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import "SBUserAccount.h"
#import "SBAddress.h"
#import "SBReceipt.h"

@implementation SBUserAccount

-(id)init {
    if ( self = [super init] ) {
        
        self.isInternalAccount = YES;
    }
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.isInternalAccount = YES;
        
        self.credits = [dict valueForKey:kCredits];
        self.dropboxEmail = [dict valueForKey:kDropboxEmail];
        self.emailPin = [[dict valueForKey:kEmailPin] intValue];
        self.accoundId = [dict valueForKey:kAccoundId];
        self.label = [dict valueForKey:kLabel];
        self.plan = [dict valueForKey:kPlan];
        self.preferredCurrency = [dict valueForKey:kPreferredCurrency];
        self.processingCenter = [dict valueForKey:kProcessingCenter];
        self.slug = [dict valueForKey:kSlug];
        
        [self parseAddressInfoFor:[dict valueForKey:kAddress]];
    }
    return self;
}

-(void) parseAddressInfoFor:(NSDictionary *)address {
    
    SBAddress *addr = [[SBAddress alloc] initWithDictionary:address];
   
    self.address = addr;
   
}

-(void) parseAccountReceipt:(NSDictionary *)result {
    
    self.accountType = @"ShoeBoxed";

    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in [result valueForKey:@"documents"]) {
        SBReceipt *receipt = [[SBReceipt alloc] initWithDictionary:dict];
        [array addObject:receipt];
    }
    
    self.receipts = array;

}

-(void) parseZiploopAccountReceipt:(NSArray *)result {
    
    self.accountType = @"";
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in result) {
        SBReceipt *receipt = [[SBReceipt alloc] initZiploopWithDictionary:dict];
        [array addObject:receipt];
    }
    
    self.receipts = array;
    
}


@end



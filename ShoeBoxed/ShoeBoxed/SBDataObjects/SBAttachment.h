//
//  SBAttachment.h
//  banking
//
//  Created by Anand kumar on 09/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBAttachment : NSObject

@property(nonatomic, copy) NSString *name;
@property(nonatomic, copy) NSString *url;
@end

//
//  SBReceipt.m
//  banking
//
//  Created by Anand kumar on 09/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import "SBReceipt.h"


@implementation SBReceipt


- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        
        self.accountId          = [dict valueForKey:kReceiptAccountId];
        self.currency           = [dict valueForKey:kReceiptCurrency];
        self.receiptId          = [dict valueForKey:kReceiptId];
        self.invoiceNumber      = [dict valueForKey:kReceiptInvoice];
        self.notes              = [dict valueForKey:kReceiptNotes];
        self.processState       = [dict valueForKey:kReceiptProState];
        //self.shareUrl           = [dict valueForKey:kReceiptShareUrl];
        self.tax                = [dict valueForKey:kReceiptTax];
        self.taxInPreCurrency   = [dict valueForKey:kReceiptTaxInPreCurrency];
        
        self.total = @"0";
        if(![[dict valueForKey:kReceiptTotal] isKindOfClass:[NSNull class]] && [dict valueForKey:kReceiptTotal] != nil){
            self.total              = [dict valueForKey:kReceiptTotal];
        }
        self.totalInPreCurrency  = [dict valueForKey:kReceiptTotalInPreCurrency];
        self.trashed            = [dict valueForKey:kReceiptTrashed];
        self.type               = [dict valueForKey:kReceiptType];
        self.vendor             = [dict valueForKey:kReceiptVendor];
        self.viewed             = [dict valueForKey:kReceiptViewed];

        //Modified Date
        //Issued Date
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        
        NSDate *issuedDate = [NSDate date];
        if(![[dict valueForKey:kReceiptIssued] isKindOfClass:[NSNull class]]){
            issuedDate = [formatter dateFromString:[dict valueForKey:kReceiptIssued]];
        }
        
        NSDate *modifiedDate = [NSDate date];
        if(![[dict valueForKey:kReceiptModified] isKindOfClass:[NSNull class]]){
            modifiedDate = [formatter dateFromString:[dict valueForKey:kReceiptModified]];
        }

        
        [formatter setDateFormat:@"MM/dd/yy"];
        
        if(issuedDate){
            self.issuedDate = issuedDate;
            self.issuedOn = [formatter stringFromDate:issuedDate];
        }
        
        if(modifiedDate) {
            self.modifiedOn = [formatter stringFromDate:modifiedDate];
        }
        
        
        formatter = nil;

        
        if(![[dict valueForKey:kReceiptAttachment] isKindOfClass:[NSNull class]]){
            [self parseAttachment:[dict valueForKey:kReceiptAttachment]];
        }
        
        if(![[dict valueForKey:kReceiptCategories] isKindOfClass:[NSNull class]]){
            [self parseCategories:[dict valueForKey:kReceiptCategories]];
        }
        
        if(![[dict valueForKey:kReceiptPayment] isKindOfClass:[NSNull class]]){
            [self parsePaymentTypes:[dict valueForKey:kReceiptPayment]];
        }

    }
    return self;
}

- (instancetype)initZiploopWithDictionary:(NSDictionary *)dict {
    
    
    self = [super init];
    if (self) {
    self.total = @"0";
        
        if(![[dict valueForKey:@"transaction_amount"] isKindOfClass:[NSNull class]] && [dict valueForKey:@"transaction_amount"] != nil){
            self.total              = [dict valueForKey:@"transaction_amount"];
        }
        
        self.vendor             = [dict valueForKey:@"store_name"];
        self.accountId          = [dict valueForKey:@"partner_record_id"];
        self.receiptId          = [dict valueForKey:@"ziploop_record_id"];
        self.type               = [dict valueForKey:@"record_type"];
        self.processState       = [dict valueForKey:@"record_status"];


        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        
        NSDate *issuedDate = [NSDate date];
        if(![[dict valueForKey:@"transaction_date"] isKindOfClass:[NSNull class]]){
            issuedDate = [formatter dateFromString:[dict valueForKey:@"transaction_date"]];
        }
        
        NSDate *modifiedDate = [NSDate date];
        if(![[dict valueForKey:@"created_at"] isKindOfClass:[NSNull class]]){
            modifiedDate = [formatter dateFromString:[dict valueForKey:@"created_at"]];
        }
        
        
        [formatter setDateFormat:@"MM/dd/yy"];
        
        if(issuedDate){
            self.issuedDate = issuedDate;
            self.issuedOn = [formatter stringFromDate:issuedDate];
        }
        
        if(modifiedDate) {
            self.modifiedOn = [formatter stringFromDate:modifiedDate];
        }
        
        formatter = nil;
        
        SBPayment *payment = [[SBPayment alloc] init];
        payment.type = @"other";
        self.payments = payment;
        
    }
    return self;
 
}


-(void) parseAttachment:(NSDictionary *)dict {
    
        SBAttachment *attachement = [[SBAttachment alloc] init];
        attachement.name = [dict valueForKey:@"name"];
        attachement.url  = [dict valueForKey:@"url"];
        
    self.attachments = attachement;    
}

-(void) parseCategories:(NSArray *)catgrylist {
    
    NSMutableArray *array = [[NSMutableArray alloc] init];

    for (NSString *catName in catgrylist) {
        
        SBCategory *cat = [[SBCategory alloc] init];
        cat.name = catName;
        
        [array addObject:cat];
    }
    
    self.categories = array;
    array = nil;
}

-(void) parsePaymentTypes:(NSDictionary *)payList {
    
    SBPayment *payment = [[SBPayment alloc] init];
    payment.type = [payList valueForKey:@"type"];
    
    if([payList valueForKey:@"lastFourDigits"]){
        payment.lastFourDigits = [payList valueForKey:@"lastFourDigits"];
    }
    
    self.payments = payment;
}

@end

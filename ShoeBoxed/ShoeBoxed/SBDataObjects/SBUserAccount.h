//
//  SBUserAccount.h
//  banking
//
//  Created by Dimple on 08/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBAddress.h"

static  NSString * const kCredits = @"credits";
static  NSString * const kDropboxEmail = @"dropboxEmail";
static  NSString * const kEmailPin = @"emailPin";
static  NSString * const kAccoundId = @"id";
static  NSString * const kLabel = @"label";
static  NSString * const kPlan = @"plan";
static  NSString * const kPreferredCurrency = @"preferredCurrency";
static  NSString * const kProcessingCenter = @"processingCenter";
static  NSString * const kSlug = @"slug";
static  NSString * const kAddress = @"address";

@interface SBUserAccount : NSObject

@property (nonatomic, copy) NSString *credits;
@property (nonatomic, copy) NSString *dropboxEmail;
@property (nonatomic, assign) int emailPin;
@property (nonatomic, copy) NSString *accoundId;
@property (nonatomic, copy) NSString *label;
@property (nonatomic, copy) NSString *plan;
@property (nonatomic, copy) NSString *preferredCurrency;
@property (nonatomic, copy) NSString *processingCenter;
@property (nonatomic, copy) NSString *slug;
@property (nonatomic, strong) SBAddress *address;
@property (nonatomic, strong) NSArray *receipts;

@property (nonatomic, copy) NSString *accountType;

@property (nonatomic, assign) BOOL isInternalAccount;

- (instancetype)initWithDictionary:(NSDictionary *)dict;
-(void) parseAccountReceipt:(NSDictionary *)result;
-(void) parseZiploopAccountReceipt:(NSArray *)result;
@end

//
//  SBUser.m
//  banking
//
//  Created by Dimple on 08/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import "SBUser.h"


@implementation SBUser

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        
        self.userid = [dict valueForKey:kUserId];
        self.email = [dict valueForKey:kUserEmail];
        self.firstName = [dict valueForKey:kUserFirstName];
        self.lastName = [dict valueForKey:kUserLastName];
        self.name = [dict valueForKey:kUserName];
        self.phone = [dict valueForKey:kUserPhone];
        self.workPhone = [dict valueForKey:kUserWorkPhone];
        self.oauth = [dict valueForKey:kUserOauth];
        self.staff = [dict valueForKey:kUserStaff];
        self.cellPhone = [dict valueForKey:kUserCellPhone];
        self.confirmed = [[dict valueForKey:kUserConfirmed] intValue];
        self.surname = [dict valueForKey:kUserSurname];
        
        [self parseAccountInfoFor:[dict valueForKey:kUserAccounts]];
        
    }
    return self;
}

- (instancetype)initZiploopWithDictionary:(NSArray *)receiptList {
    
    self = [super init];
    if (self) {

        
        self.userid = @"";
        self.email = @"";
        self.firstName = @"ziploop";
        self.lastName = @"ziploop";
        self.name = @"ziploop";
        self.phone = @"";
        self.workPhone = @"";
        
        SBUserAccount *account = [[SBUserAccount alloc] init];
        account.slug = @"Ziploop";
        [account parseZiploopAccountReceipt:receiptList];
        self.accounts = @[account];

    }
    
    return self;
}

-(void) parseAccountInfoFor:(NSArray *)accList {
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dic in accList) {
        
        SBUserAccount *account = [[SBUserAccount alloc] initWithDictionary:dic];
        [array addObject:account];
    }
    
    self.accounts = array;
    
    array = nil;
}


@end


//
//  SBAddress.h
//  banking
//
//  Created by Dimple on 08/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import <Foundation/Foundation.h>

static  NSString * const kCity = @"city";
static  NSString * const kCountry = @"country";
static  NSString * const kRegion = @"region";
static  NSString * const kPostcode = @"postcode";
static  NSString * const kStreetLine1 = @"streetLine1";
static  NSString * const kStreetLine2 = @"streetLine2";
static  NSString * const kFirstName = @"firstName";
static  NSString * const kLastName= @"lastName";
static  NSString * const kName = @"name";
static  NSString * const kSurname = @"surname";

@interface SBAddress : NSObject

@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSArray *region;
@property (nonatomic, copy) NSString *postcode;
@property (nonatomic, copy) NSString *streetLine1;
@property (nonatomic, copy) NSString *streetLine2;
@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *surname;

- (instancetype)initWithDictionary:(NSDictionary *)dict;
@end

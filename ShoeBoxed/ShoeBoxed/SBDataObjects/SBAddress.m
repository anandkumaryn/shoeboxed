//
//  SBAddress.m
//  banking
//
//  Created by Dimple on 08/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import "SBAddress.h"

@implementation SBAddress

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        
        self.city = [dict valueForKey:kCity];
        self.country = [dict valueForKey:kCountry];
        self.region = [dict valueForKey:kRegion];
        self.postcode = [dict valueForKey:kPostcode];
        self.firstName = [dict valueForKey:kFirstName];
        self.lastName = [dict valueForKey:kLastName];
        self.name = [dict valueForKey:kName];
        self.surname = [dict valueForKey:kSurname];
        self.streetLine1 = [dict valueForKey:kStreetLine1];
        self.streetLine2 = [dict valueForKey:kStreetLine2];
        
    }
    return self;
}

@end



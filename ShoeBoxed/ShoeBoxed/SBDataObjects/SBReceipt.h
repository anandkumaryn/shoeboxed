//
//  SBReceipt.h
//  banking
//
//  Created by Anand kumar on 09/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBAttachment.h"
#import "SBCategory.h"
#import "SBPayment.h"


static  NSString * const kReceiptId = @"id";
static  NSString * const kReceiptAccountId = @"accountId";
static  NSString * const kReceiptCurrency = @"currency";
static  NSString * const kReceiptInvoice = @"invoiceNumber";
static  NSString * const kReceiptIssued = @"issued";
static  NSString * const kReceiptModified = @"modified";
static  NSString * const kReceiptNotes = @"noted";
static  NSString * const kReceiptProState = @"processingState";
static  NSString * const kReceiptShareUrl = @"shareUrl";
static  NSString * const kReceiptTax = @"tax";
static  NSString * const kReceiptTaxInPreCurrency = @"taxInPreferredCurrency";
static  NSString * const kReceiptTotal = @"total";
static  NSString * const kReceiptTotalInPreCurrency = @"totalInPreferredCurrency";
static  NSString * const kReceiptTrashed = @"trashed";
static  NSString * const kReceiptType = @"type";
static  NSString * const kReceiptVendor = @"vendor";
static  NSString * const kReceiptViewed = @"viewed";
static  NSString * const kReceiptInvoiceNumber = @"invoiceNumber";

static  NSString * const kReceiptAttachment = @"attachment";
static  NSString * const kReceiptCategories = @"categories";
static  NSString * const kReceiptPayment = @"paymentType";



@interface SBReceipt : NSObject

@property(nonatomic, copy) NSString *receiptId;
@property(nonatomic, copy) NSString *accountId;
@property(nonatomic, copy) NSString *currency;
@property(nonatomic, copy) NSString *issuedOn;
@property(nonatomic, strong) NSDate *issuedDate;

@property(nonatomic, copy) NSString *modifiedOn;
@property(nonatomic, copy) NSString *notes;
@property(nonatomic, copy) NSString *processState;
@property(nonatomic, copy) NSString *shareUrl;
@property(nonatomic, copy) NSString *tax;
@property(nonatomic, copy) NSString *taxInPreCurrency;
@property(nonatomic, copy) NSString *total;
@property(nonatomic, copy) NSString *totalInPreCurrency;
@property(nonatomic, copy) NSString *trashed;
@property(nonatomic, copy) NSString *type;
@property(nonatomic, copy) NSString *vendor;
@property(nonatomic, copy) NSString *viewed;
@property(nonatomic, copy) NSString *invoiceNumber;

@property(nonatomic, strong) SBAttachment *attachments;
@property(nonatomic, strong) NSArray *categories;
@property(nonatomic, strong) SBPayment *payments;

@property(nonatomic, assign) BOOL isSelected;

- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (instancetype)initZiploopWithDictionary:(NSDictionary *)dict;



@end

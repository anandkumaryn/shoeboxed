//
//  SBPayment.h
//  banking
//
//  Created by Anand kumar on 09/03/17.
//  Copyright © 2017 First Republic Bank. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBPayment : NSObject

@property(nonatomic, copy) NSString *type;
@property(nonatomic, copy) NSString *lastFourDigits;

@end
